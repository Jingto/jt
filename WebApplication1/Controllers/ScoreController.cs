﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View(new scoredata());
        }
        [HttpPost]
        public ActionResult Index(scoredata data)
        {
            float score = data.score;
            string level = "";
            string w = "";
            if (score < 19 && score >= 0)
            {
                level = "E";
            }
            else if (score < 39 && score >= 20)
            {
                level = "D";
            }
            else if (score < 59 && score >= 40)
            {
                level = "C";
            }
            else if (score < 79 && score >= 60)
            {
                level = "B";
            }
            else if (score <= 100 && score >= 80)
            {
                level = "A";
            }
            if(score<0 || score > 100)
            {
                w = "請輸入0-100的分數";
            }
            data.W = w;
            data.score = score;
            data.level = level;
            return View(data);
        }
    }
}