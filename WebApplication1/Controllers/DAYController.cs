﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class DAYController : Controller
    {
        // GET: DAY
        public ActionResult Index()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Index(float Day)
        {
            string DAY = "";
            if (Day == 1)
            {
                DAY = "星期一";
            }
            else if (Day == 2)
            {
                DAY = "星期二";
            }
            else if (Day == 3)
            {
                DAY = "星期三";
            }
            else if (Day == 4)
            {
                DAY = "星期四";
            }
            else if (Day == 5)
            {
                DAY = "星期五";
            }
            else if (Day == 6)
            {
                DAY = "星期六";
            }
            else if (Day == 7)
            {
                DAY = "星期日";
            }
            else
            {
                DAY = "請輸入1-7的數字";
            }
            ViewBag.DAY = DAY;
            return View();
        }
    }
}