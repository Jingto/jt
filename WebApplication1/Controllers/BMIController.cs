﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIdata());
        }
        [HttpPost]
        public ActionResult Index(BMIdata data)
        {
            if (ModelState.IsValid)
            {
                float m_height = data.Height / 100;
                float BMI = data.Weight / (m_height * m_height);
                string h = "";
                string w = "";
                string level = "";
                if (BMI < 18.5)
                {
                    level = "體重過輕";
                }
                else if (18.5 <= BMI && BMI < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= BMI && BMI < 27)
                {
                    level = "過重";
                }
                else if (27 <= BMI && BMI < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= BMI && BMI < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= BMI)
                {
                    level = "重度肥胖";
                }
                if (data.Height < 100 || data.Height > 250)
                {
                    h = "請輸入100-250的數字";
                }
                if (data.Weight < 30 || data.Weight > 200)
                {
                    w = "請輸入30-200的數字";
                }
                data.BMI = BMI;
                data.Level = level;
            }

            return View(data);
        }

    }
}