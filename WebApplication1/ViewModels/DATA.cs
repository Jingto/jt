﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class BMIdata
    {
        
        [Display(Name = "體重")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 200, ErrorMessage = "請輸入30-200的數值")]
        public float Weight { get; set; }
        [Display(Name = "身高")]
        [Required(ErrorMessage = "必填欄位")]
        [Range(30, 200, ErrorMessage = "請輸入50-250的數值")]
        public float Height { get; set; }
        public float? BMI { get; set; }
        public string Level { get; set; }
    }
    public class scoredata
    {
        public float score { get; set; }
        public string W { get; set; }
        public string level { get; set; }
    }
}